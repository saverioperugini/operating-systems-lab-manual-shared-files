/* ref. http://www.cs.appstate.edu/~jtw/cs3481/labs/fork.html */

#include<stdio.h>
#include<unistd.h>
main () {

   fork();
   fork();
   fork();
   fprintf (stderr, "I am process %ld and my parent is %ld.\n", (long) getpid(),
           (long) getppid());
}
