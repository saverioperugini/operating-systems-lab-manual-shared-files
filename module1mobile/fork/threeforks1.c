/* ref. Robbins and Robbins' webpage:
   http://vip.cs.utsa.edu/classes/cs3733s2004/notes/USP-03.html */

#include<stdio.h>
#include<unistd.h>
main () {

   int c2 = 0;
   int c1 = fork();      /* fork number 1 */
   if (c1 == 0)
      c2 = fork();   /* fork number 2 */
   fork();           /* fork number 3 */
   if (c2 > 0)
      fork();        /* fork number 4 */

   fprintf (stderr, "I am process %ld and my parent is %ld.\n", (long) getpid(),
           (long) getppid());
}
