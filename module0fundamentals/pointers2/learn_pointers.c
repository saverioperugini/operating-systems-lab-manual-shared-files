#include<stdio.h>

main() {

   int a = 1;

   int* ptrtoa = &a;
   int** ptr2ptrtoa = &ptrtoa;

   printf ("a is at %x.\n", &a);
   printf ("ptrtoa points to %x.\n", ptrtoa);
   printf ("ptrtoa is at %x.\n", &ptrtoa);

   printf ("ptr2ptrtoa points to %x.\n", ptr2ptrtoa);
   printf ("what ptr2ptrtoa points to points to %x.\n", *ptr2ptrtoa);
   printf ("what ptr2ptrtoa points to points to contains %x.\n", **ptr2ptrtoa);
}
